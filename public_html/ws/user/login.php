<?php

/**
 * Logea un usuario
 */
require_once '../db/model/sis_user.php';
require_once '../service.php';
header('Content-type: application/json');


$method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : null;

if ($method == "POST") {
    $inputJSON = file_get_contents('php://input');
    $input = json_decode($inputJSON, FALSE);
    if (!isset($input->name) || !isset($input->pass)) {
        service::setResponseAndExit(HTTPCodes::Bad_Request, MsgStatus::ERROR, "Input Syntax error or invalid fields");
    }
    $u = new sis_user();
    $u->name = $input->{"name"};
    $u->pass = $input->{"pass"};
    $sql = db::getInstanceDefault();
    if (!$sql) {
        service::setResponseAndExit(HTTPCodes::Internal_Server_Error, MsgStatus::ERROR, 'No database');
    }
    $res = sis_user::login($sql, $u);
    $sql->close();
    if ($res) {
        service::setResponse(HTTPCodes::Created, MsgStatus::OK, "User Loged", $res);
    } else {
        service::setResponse(HTTPCodes::Unauthorized, MsgStatus::ERROR, 'User credentials error');
    }
    return;
}
service::setResponse(HTTPCodes::Method_Not_Allowed, MsgStatus::ERROR, 'This method type is not allowed');
