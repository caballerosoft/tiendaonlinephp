<?php

require_once dirname(__DIR__) . '../db.php';

/**
 * Description of promotion
 *
 * @author HadesDX
 */
class promotion {

    public $id_promotion;
    public $discount;
    public $begin;
    public $end;
    public $status;
    public $id_item;

    /**
     * 
     * @param mysqli $sql Conexion de base de datos
     * @param int $id id
     * @return promotion
     */
    public static function findById($sql, $id) {
        if (!$sql || !$id) {
            return null;
        }
        try {
            $o = new promotion();
            $res = ($stmt = $sql->prepare("select * from `promotion` where id_promotion=?")) && true;
            $res &= $stmt->bind_param("i", $id);
            $res &= $stmt->execute();
            $res &= $stmt->bind_result($o->id_promotion, $o->discount, $o->begin, $o->end, $o->status, $o->id_item);
            $res &= $stmt->store_result();
            $resFetch = $stmt->fetch();
            if (!$res || $resFetch === false) {
                return null;
            }
            return $o;
        } catch (Exception $e) {
            //echo $e;
        } finally {
            $stmt->close();
        }
        return null;
    }

    /**
     * Find best promotion active for the item
     * @param mysqli $sql Conexion de base de datos
     * @param int $id item
     * @return promotion
     */
    public static function findBestByItemId($sql, $id) {
        if (!$sql || !$id) {
            return null;
        }
        try {
            $o = new promotion();
            $res = ($stmt = $sql->prepare("select * from `promotion` where `id_item`=? and `status`=true and NOW() between `begin` and `end` order by discount limit 1")) && true;
            $res &= $stmt->bind_param("i", $id);
            $res &= $stmt->execute();
            $res &= $stmt->bind_result($o->id_promotion, $o->discount, $o->begin, $o->end, $o->status, $o->id_item);
            $resFetch = $stmt->fetch();
            if (!$res || !$resFetch) {
                return null;
            }
            return $o;
        } catch (Exception $e) {
            //echo $e;
        } finally {
            $stmt->close();
        }
        return null;
    }

}
