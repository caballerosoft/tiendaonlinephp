<?php

/**
 * Description of db
 *
 * @author HadesDX
 */
class db {

    public $dbConfig;
    private static $instance;

    private function __construct() {
        include_once 'config.php';
        $this->dbConfig = $dbConfig;
    }

    /**
     * Obtener una conecion de base de datos por argumentos
     * 
     * @return \mysqli
     */
    public static function getInstance($host, $user, $passwd, $database) {
        $sql = new mysqli($host, $user, $passwd, $database);
        if ($sql->connect_errno) {
            throw new Exception($sql->connect_errno . ": " . $sql->connect_error);
        }
        return $sql;
    }

    /**
     * Obtener una conecion de base de datos por default
     * 
     * @return \mysqli
     */
    public static function getInstanceDefault() {
        if (!db::$instance) {
            db::$instance = new db();
        }
        return db::getInstance(db::$instance->dbConfig->host, db::$instance->dbConfig->user, db::$instance->dbConfig->passwd, db::$instance->dbConfig->database);
    }

    public static function setDBDefault($host, $user, $passwd, $database) {
        db::$instance->$dbConfig->host = $host;
        db::$instance->$dbConfig->user = $user;
        db::$instance->$dbConfig->passwd = $passwd;
        db::$instance->$dbConfig->database = $database;
    }

}
