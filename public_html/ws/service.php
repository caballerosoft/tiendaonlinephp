<?php

abstract class HTTPCodes {

// 1XX Informational
    const Continue_ = 100;
    const Switching_Protocols = 101;
    const Processing = 102;
// 2XX Success
    const OK = 200;
    const Created = 201;
    const Accepted = 202;
    const Non_authoritative_Information = 203;
    const No_Content = 204;
    const Reset_Content = 205;
    const Partial_Content = 206;
    const Multi_Status = 207;
    const IM_Used = 226;
// 3XX Redirection
    const Multiple_Choices = 300;
    const Moved_Permanently = 301;
    const Found = 302;
    const See_Other = 303;
    const Not_Modified = 304;
    const Use_Proxy = 305;
    const Temporary_Redirect = 307;
    const Permanent_Redirect = 308;
// 4XX Client_Error
    const Bad_Request = 400;
    const Unauthorized = 401;
    const Payment_Required = 402;
    const Forbidden = 403;
    const Not_Found = 404;
    const Method_Not_Allowed = 405;
    const Not_Acceptable = 406;
    const Proxy_Authentication_Required = 407;
    const Request_Timeout = 408;
    const Conflict = 409;
    const Gone = 410;
    const Length_Required = 411;
    const Precondition_Failed = 412;
    const Payload_Too_Large = 413;
    const Request_URI_Too_Long = 414;
    const Unsupported_Media_Type = 415;
    const Requested_Range_Not_Satisfiable = 416;
    const Expectation_Failed = 417;
    const Im_a_teapot = 418;
    const Misdirected_Request = 421;
    const Unprocessable_Entity = 422;
    const Locked = 423;
    const Failed_Dependency = 424;
    const Upgrade_Required = 426;
    const Precondition_Required = 428;
    const Too_Many_Requests = 429;
    const Request_Header_Fields_Too_Large = 431;
    const Connection_Closed_Without_Response = 444;
    const Unavailable_For_Legal_Reasons = 451;
    const Client_Closed_Request = 499;
// 5XX Server_Error
    const Internal_Server_Error = 500;
    const Not_Implemented = 501;
    const Bad_Gateway = 502;
    const Service_Unavailable = 503;
    const Gateway_Timeout = 504;
    const HTTP_Version_Not_Supported = 505;
    const Variant_Also_Negotiates = 506;
    const Insufficient_Storage = 507;
    const Loop_Detected = 508;
    const Not_Extended = 510;
    const Network_Authentication_Required = 511;
    const Network_Connect_Timeout_Error = 599;

}

abstract class MsgStatus {

    const ERROR = "Error";
    const OK = "OK";

}

/**
 * Description of service
 *
 * @author HadesDX
 */
class service {

    /**
     * 
     * @param HTTPCodes $code
     * @param MsgStatus $status
     * @param string $msg
     * @param object $data
     */
    public static function setResponse($code, $status, $msg, $data = null) {
        http_response_code($code);
        $obj = new stdClass();
        $obj->status = $status;
        $obj->msg = $msg;
        $obj->data = $data;
        echo json_encode($obj);
    }

    public static function setResponseAndExit($code, $status, $msg, $data = null) {
        self::setResponse($code, $status, $msg, $data);
        exit();
    }

}
