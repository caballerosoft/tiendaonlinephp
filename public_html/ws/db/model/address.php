<?php

require_once dirname(__DIR__) . '../db.php';

/**
 * Description of address
 *
 * @author HadesDX
 */
class address {

    //PRIMARY KEY
    public $id_address;
    public $cp;
    public $street;
    public $number;
    public $state;
    public $city;
    public $community;
    public $reference;
    //FOREIGN KEY
    public $id_user;

    public function __clone() {
        
    }

    /**
     * Construye el objeto address desde un objecto de json_decode
     * @param object $v
     * @return \address
     */
    public static function fromVar($v) {
        if (!$v) {
            return null;
        }
        $o = new address();
        if (isset($v->id_address)) {
            $o->id_address = $v->id_address;
        }
        if (isset($v->cp)) {
            $o->cp = $v->cp;
        }
        if (isset($v->street)) {
            $o->street = $v->street;
        }
        if (isset($v->number)) {
            $o->number = $v->number;
        }
        if (isset($v->state)) {
            $o->state = $v->state;
        }
        if (isset($v->city)) {
            $o->city = $v->city;
        }
        if (isset($v->community)) {
            $o->community = $v->community;
        }
        if (isset($v->reference)) {
            $o->reference = $v->reference;
        }
        if (isset($v->id_user)) {
            $o->id_user = $v->id_user;
        }

        return $o;
    }

    /**
     * Valida la estructura para ser usada en la base de datos.
     * No valida llaves.
     * 
     * @param address $o Instancia a validar
     * @return boolean true on valid
     */
    public static function validate($o = null) {
        if (!$o || $o->cp == null || $o->number == null) {
            return false;
        }
        return true;
    }

    /**
     * 
     * @param mysqli $sql Conexion de base de datos
     * @param int $id id
     * @return boolean
     */
    public static function dropById($sql = null, $id = null) {
        if (!$sql || !$id) {
            return false;
        }
        try {
            $res = ($stmt = $sql->prepare("delete from `address` where id_address=?")) && true;
            $res &= $stmt->bind_param("i", $id);
            $res &= $stmt->execute();
            if (!$res) {
                return false;
            }
            return true;
        } catch (Exception $e) {
            //echo $e;
        } finally {
            $stmt->close();
        }
        return false;
    }

    /**
     * 
     * @param mysqli $sql Conexion de base de datos
     * @param int $id id
     * @return address
     */
    public static function findById($sql, $id) {
        if (!$sql || !$id) {
            return null;
        }
        try {
            $o = new address();
            $res = ($stmt = $sql->prepare("select * from `address` where id_address=?")) && true;
            $res &= $stmt->bind_param("i", $id);
            $res &= $stmt->execute();
            $res &= $stmt->bind_result($o->id_address, $o->cp, $o->street, $o->number, $o->state, $o->city, $o->community, $o->reference, $o->id_user);
            $resFetch = $stmt->fetch();
            if (!$res || $resFetch === false) {
                return null;
            }
            return $o;
        } catch (Exception $e) {
            //echo $e;
        } finally {
            $stmt->close();
        }
        return null;
    }

    /**
     * 
     * @param mysqli $sql Conexion de base de datos
     * @param int $id id user
     * @return array of address
     */
    public static function findByUserId($sql, $id) {
        if (!$sql || !$id) {
            return null;
        }
        try {
            $o = new address();
            $res = ($stmt = $sql->prepare("select * from `address` where id_user=?")) && true;
            $res &= $stmt->bind_param("i", $id);
            $res &= $stmt->execute();
            $res &= $stmt->bind_result($o->id_address, $o->cp, $o->street, $o->number, $o->state, $o->city, $o->community, $o->reference, $o->id_user);
            if (!$res) {
                return null;
            }
            $array = array();
            while ($resFetch = $stmt->fetch()) {
                //TODO FIND A BETTER WAY TO CLONE THIS!
                $clone = unserialize(serialize($o));
                array_push($array, $clone);
                if ($resFetch === false) {
                    return null;
                }
            }
            return $array;
        } catch (Exception $e) {
            //echo $e;
        } finally {
            $stmt->close();
        }
        return null;
    }

    /**
     * @param mysqli $sql
     * @param address $o
     * @return int id generado 
     */
    public static function create($sql, $o) {
        if (!self::validate($o)) {
            return null;
        }
        try {
            $res = ($stmt = $sql->prepare("INSERT INTO `address` (`cp`, `street`, `number`, `state`, `city`, `community`, `reference`, `id_user`) VALUES (?, ?, ?, ?, ?, ?, ?, ?)")) && true;
            $res &= $stmt->bind_param("sssssssi", $o->cp, $o->street, $o->number, $o->state, $o->city, $o->community, $o->reference, $o->id_user);
            $res &= $stmt->execute();
            if (!$res) {
                return null;
            }
            $id = $stmt->insert_id;
            return $id;
        } catch (Exception $e) {
            //echo $e;
        } finally {
            $stmt->close();
        }
        return null;
    }

    /**
     * @param mysqli $sql Conexion de base de datos
     * @param address $o Objeto con datos del usuario
     * @return boolean
     */
    public static function update($sql, $o) {
        if (!self::validate($o) || !$o->id_address || !$o->id_user) {
            return false;
        }
        try {
            $res = ($stmt = $sql->prepare("UPDATE `address` set `cp`=?, `street`=?, `number`=?, `state`=?, `city`=?, `community`=?, `reference`=?, `id_user`=? where id_address=?")) && true;
            $res &= $stmt->bind_param("sssssssii", $o->cp, $o->street, $o->number, $o->state, $o->city, $o->community, $o->reference, $o->id_user, $o->id_address);
            $res &= $stmt->execute();
            return $res;
        } catch (Exception $e) {
            //echo $e;
        } finally {
            if ($stmt) {
                $stmt->close();
            }
        }
        return false;
    }

}
