<?php

/**
 * 
 */
require_once '../db/model/sis_user_data.php';
require_once '../service.php';
header('Content-type: application/json');


$method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : null;

if ($method == "POST") {
    //Parse Input
    $inputJSON = file_get_contents('php://input');
    $input = json_decode($inputJSON, FALSE);

    if (!$input) {
        service::setResponseAndExit(HTTPCodes::Bad_Request, MsgStatus::ERROR, 'Parameter syntax error');
    }
    $o = sis_user_data::fromVar($input);
    if (!$o || $o->id_user_data == null) {
        service::setResponseAndExit(HTTPCodes::Bad_Request, MsgStatus::ERROR, 'Required parameters not found or invalid');
    }

    $sql = db::getInstanceDefault();
    if (!$sql) {
        service::setResponseAndExit(HTTPCodes::Bad_Request, MsgStatus::ERROR, 'No database');
    }
    $res = sis_user_data::findById($sql, $o->id_user_data);
    $sql->close();
    if ($res) {
        if (!$res->id_user_data) {
            $res = null;
        }
        service::setResponse(HTTPCodes::OK, MsgStatus::OK, 'User data', $res);
    } else {
        service::setResponse(HTTPCodes::Internal_Server_Error, MsgStatus::ERROR, 'User data error at search');
    }
    return;
}
service::setResponse(HTTPCodes::Method_Not_Allowed, MsgStatus::ERROR, 'This method type is not allowed');
