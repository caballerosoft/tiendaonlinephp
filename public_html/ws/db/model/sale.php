<?php

require_once dirname(__DIR__) . '/model/sis_user.php';
require_once dirname(__DIR__) . '../db.php';

abstract class status {

    const SHOPPING = 'shopping';
    const PROCESO = 'proceso';
    const AUTORIZADO = 'autorizado';
    const SURTIDO = 'surtido';

}

/**
 * Description of sale
 *
 * @author HadesDX
 */
class sale {

    public $id_sale;
    public $id_user;
    public $date;
    public $status;
    public $num_bill;

    /**
     * Construye el objeto sis_category desde un objecto de json_decode
     * @param object $v
     * @return \sale
     */
    public static function fromVar($v) {
        if (!$v) {
            return null;
        }
        $o = new sale();
        if (isset($v->id_sale)) {
            $o->id_sale = $v->id_sale;
        }
        if (isset($v->id_user)) {
            $o->id_user = $v->id_user;
        }
        if (isset($v->date)) {
            $o->date = $v->date;
        }
        if (isset($v->status)) {
            $o->status = $v->status;
        }
        if (isset($v->num_bill)) {
            $o->num_bill = $v->num_bill;
        }
        return $o;
    }

    /**
     * Valida la estructura para ser usada en la base de datos.
     * No valida llaves.
     * 
     * @param sale $o Instancia a validar
     * @return boolean true on valid
     */
    public static function validate($o = null) {
        if (!$o || $o->status == null) {
            return false;
        }
        return true;
    }

    /**
     * 
     * @param mysqli $sql Conexion de base de datos
     * @param int $id id
     * @return sale
     */
    public static function findById($sql, $id) {
        if (!$sql || !$id) {
            return null;
        }
        try {
            $o = new sale();
            $res = ($stmt = $sql->prepare("select * from `sale` where id_sale=?")) && true;
            $res &= $stmt->bind_param("i", $id);
            $res &= $stmt->execute();
            $res &= $stmt->bind_result($o->id_sale, $o->id_user, $o->date, $o->status, $o->num_bill);
            $res &= $stmt->store_result();
            $resFetch = $stmt->fetch();
            if (!$res || $resFetch === false) {
                return null;
            }
            return $o;
        } catch (Exception $e) {
            //echo $e;
        } finally {
            $stmt->close();
        }
        return null;
    }

    /**
     * 
     * @param mysqli $sql Conexion de base de datos
     * @return array of sale
     */
    public static function findAll($sql) {
        if (!$sql) {
            return null;
        }
        try {
            $o = new sale();
            $res = ($stmt = $sql->prepare("SELECT * FROM `sale` order by `date` desc")) && true;
            $res &= $stmt->execute();
            $res &= $stmt->bind_result($o->id_sale, $o->id_user, $o->date, $o->status, $o->num_bill);
            $res &= $stmt->store_result();
            if (!$res) {
                return null;
            }
            $array = array();
            while ($resFetch = $stmt->fetch()) {
                if ($resFetch === false) {
                    return null;
                }
                //TODO FIND A BETTER WAY TO CLONE THIS!
                $clone = unserialize(serialize($o));
                $clone->user = sis_user::findById($sql, $o->id_user);
                $clone->user->pass = null;
                array_push($array, $clone);
            }
            return $array;
        } catch (Exception $e) {
            //echo $e;
        } finally {
            $stmt->close();
        }
        return null;
    }

    /**
     * 
     * @param mysqli $sql Conexion de base de datos
     * @return array of sale
     */
    public static function findOpen($sql, $id) {
        if (!$sql || !$id) {
            return null;
        }
        try {
            $o = new sale();
            $res = ($stmt = $sql->prepare("SELECT * FROM `sale` where id_user=? and status != 'surtido' order by `date` desc ")) && true;
            $res &= $stmt->bind_param("i", $id);
            $res &= $stmt->execute();
            $res &= $stmt->bind_result($o->id_sale, $o->id_user, $o->date, $o->status, $o->num_bill);
            $res &= $stmt->store_result();
            if (!$res) {
                return null;
            }
            $array = array();
            while ($resFetch = $stmt->fetch()) {
                if ($resFetch === false) {
                    return null;
                }
                //TODO FIND A BETTER WAY TO CLONE THIS!
                $clone = unserialize(serialize($o));
                array_push($array, $clone);
            }
            return $array;
        } catch (Exception $e) {
            //echo $e;
        } finally {
            $stmt->close();
        }
        return null;
    }

    /**
     * 
     * @param mysqli $sql Conexion de base de datos
     * @return array of sale
     */
    public static function findArchived($sql, $id) {
        if (!$sql || !$id) {
            return null;
        }
        try {
            $o = new sale();
            $res = ($stmt = $sql->prepare("SELECT * FROM `sale` where id_user=? and status = 'surtido' order by `date` desc ")) && true;
            $res &= $stmt->bind_param("i", $id);
            $res &= $stmt->execute();
            $res &= $stmt->bind_result($o->id_sale, $o->id_user, $o->date, $o->status, $o->num_bill);
            $res &= $stmt->store_result();
            if (!$res) {
                return null;
            }
            $array = array();
            while ($resFetch = $stmt->fetch()) {
                if ($resFetch === false) {
                    return null;
                }
                //TODO FIND A BETTER WAY TO CLONE THIS!
                $clone = unserialize(serialize($o));
                array_push($array, $clone);
            }
            return $array;
        } catch (Exception $e) {
            //echo $e;
        } finally {
            $stmt->close();
        }
        return null;
    }

    /**
     * 
     * @param mysqli $sql
     * @param sale $o
     * @return int id generado 
     */
    public static function create($sql, $o) {
        if (!self::validate($o)) {
            return null;
        }
        try {
            $res = ($stmt = $sql->prepare("INSERT INTO `sale` (`id_user`, `date`, `status`, `num_bill`) VALUES (?, NOW(), ?, ?)")) && true;
            $res &= $stmt->bind_param("iss", $o->id_user, $o->status, $o->num_bill);
            $res &= $stmt->execute();
            if (!$res) {
                return null;
            }
            $id = $stmt->insert_id;
            return $id;
        } catch (Exception $e) {
            //echo $e;
        } finally {
            $stmt->close();
        }
        return null;
    }

    /**
     * 
     * @param mysqli $sql
     * @param sale $o
     * @return boolean|null
     */
    public static function setStatus($sql, $id, $status) {
        if (!$sql || !$id || !$status) {
            return null;
        }
        try {
            $res = ($stmt = $sql->prepare("update `sale` set `status`=? where id_sale=?")) && true;
            $res &= $stmt->bind_param("si", $status, $id);
            $res &= $stmt->execute();
            return $res;
        } catch (Exception $e) {
            //echo $e;
        } finally {
            $stmt->close();
        }
        return false;
    }

}
