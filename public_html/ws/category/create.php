<?php

/**
 * 
 */
require_once '../db/model/sis_category.php';
require_once '../service.php';
header('Content-type: application/json');


$method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : null;

if ($method == "POST") {
    //Parse Input
    $inputJSON = file_get_contents('php://input');
    $input = json_decode($inputJSON, FALSE);

    if (!$input) {
        service::setResponseAndExit(HTTPCodes::Bad_Request, MsgStatus::ERROR, 'Parameter syntax error');
    }
    $o = sis_category::fromVar($input);

    if (!sis_category::validate($o)) {
        service::setResponseAndExit(HTTPCodes::Bad_Request, MsgStatus::ERROR, 'Required parameters not found or invalid');
    }

    $sql = db::getInstanceDefault();
    if (!$sql) {
        service::setResponseAndExit(HTTPCodes::Internal_Server_Error, MsgStatus::ERROR, 'No database');
    }

    $res = sis_category::create($sql, $o);

    $sql->close();
    if ($res) {
        service::setResponse(HTTPCodes::OK, MsgStatus::OK, "Category created", $res);
    } else {
        service::setResponse(HTTPCodes::Internal_Server_Error, MsgStatus::ERROR, "Server error at create");
    }
    return;
}
service::setResponse(HTTPCodes::Method_Not_Allowed, MsgStatus::ERROR, 'This method type is not allowed');
