<?php

require_once dirname(__DIR__) . '/model/promotion.php';
require_once dirname(__DIR__) . '../db.php';

/**
 * Description of sis_item
 *
 * @author HadesDX
 */
class sis_item {

    //PRIMARY KEY
    public $id_item;
    public $name;
    public $number;
    public $description;
    public $available;
    //FOREIGN KEY
    public $id_category;
    public $file_image;
    public $cost;

    public function __clone() {
        
    }

    /**
     * Construye el objeto address desde un objecto de json_decode
     * @param object $v
     * @return \address
     */
    public static function fromVar($v) {
        if (!$v) {
            return null;
        }
        $o = new sis_item();
        if (isset($v->id_item)) {
            $o->id_item = $v->id_item;
        }
        if (isset($v->name)) {
            $o->name = $v->name;
        }
        if (isset($v->number)) {
            $o->number = $v->number;
        }
        if (isset($v->description)) {
            $o->description = $v->description;
        }
        if (isset($v->available)) {
            $o->available = $v->available;
        }
        if (isset($v->id_category)) {
            $o->id_category = $v->id_category;
        }
        if (isset($v->file_image)) {
            $o->file_image = $v->file_image;
        }
        if (isset($v->cost)) {
            $o->cost = $v->cost;
        }

        return $o;
    }

    /**
     * Valida la estructura para ser usada en la base de datos.
     * No valida llaves.
     * 
     * @param sis_item $o Instancia a validar
     * @return boolean true on valid
     */
    public static function validate($o = null) {
        if (!$o || $o->name == null || !$o->id_category || $o->number === null || $o->number === '' || $o->available === null || $o->available === '') {
            return false;
        }
        return true;
    }

    /**
     * //TODO Adaptar a esta clase
     * @param mysqli $sql Conexion de base de datos
     * @param int $id id
     * @return boolean
     */
    public static function dropById($sql = null, $id = null) {
        if (!$sql || !$id) {
            return false;
        }
        try {
            $res = ($stmt = $sql->prepare("delete from `address` where id_address=?")) && true;
            $res &= $stmt->bind_param("i", $id);
            $res &= $stmt->execute();
            if (!$res) {
                return false;
            }
            return true;
        } catch (Exception $e) {
            //echo $e;
        } finally {
            $stmt->close();
        }
        return false;
    }

    /**
     * //TODO  Adaptar a esta clase
     * @param mysqli $sql Conexion de base de datos
     * @param int $id id
     * @return address
     */
    public static function findById($sql, $id) {
        if (!$sql || !$id) {
            return null;
        }
        try {
            $o = new sis_item();
            $res = ($stmt = $sql->prepare("select * from `sis_item` where id_item=?")) && true;
            $res &= $stmt->bind_param("i", $id);
            $res &= $stmt->execute();
            $res &= $stmt->bind_result($o->id_item, $o->name, $o->number, $o->description, $o->available, $o->id_category, $o->file_image, $o->cost);
            $res &= $stmt->store_result();
            $resFetch = $stmt->fetch();
            if (!$res || $resFetch === false) {
                return null;
            }
            return $o;
        } catch (Exception $e) {
            //echo $e;
        } finally {
            $stmt->close();
        }
        return null;
    }

    /**
     * 
     * @param mysqli $sql Conexion de base de datos
     * @param int $id id user
     * @return array of sis_item
     */
    public static function search($sql, $q) {
        if (!$sql || !$q) {
            return null;
        }

        try {
            $o = new sis_item();
            $res = ($stmt = $sql->prepare("select i.* from sis_item i left outer join sis_category c on i.id_category=c.id_category  where i.name like CONCAT('%', ?, '%') or i.description like CONCAT('%', ?, '%') or  c.name like CONCAT('%', ?, '%') or  c.subcategoria like CONCAT('%', ?, '%')")) && true;
            echo $sql->error;
            $res &= $stmt->bind_param("ssss", $q, $q, $q, $q);
            $res &= $stmt->execute();
            $res &= $stmt->store_result();
            $res &= $stmt->bind_result($o->id_item, $o->name, $o->number, $o->description, $o->available, $o->id_category, $o->file_image, $o->cost);
            if (!$res) {
                return null;
            }
            $array = array();
            while (true) {
                $resFetch = $stmt->fetch();
                if ($resFetch === false) {
                    return null;
                } else if ($resFetch === null) {
                    break;
                }
                //TODO FIND A BETTER WAY TO CLONE THIS!
                $clone = unserialize(serialize($o));
                $promo = promotion::findBestByItemId($sql, $clone->id_item);
                $category = sis_category::findById($sql, $clone->id_category);
                if ($promo) {
                    $clone->promotion = $promo;
                }
                if ($category) {
                    $clone->category = $category;
                }
                array_push($array, $clone);
            }
            return $array;
        } catch (Exception $e) {
            //echo $e;
        } finally {
            $stmt->close();
        }
        return null;
    }

    /**
     * 
     * @param mysqli $sql Conexion de base de datos
     * @param int $id id user
     * @return array of sis_item
     */
    public static function findRecent($sql, $qty) {
        if (!$sql || !$qty) {
            return null;
        }
        try {
            $o = new sis_item();
            $res = ($stmt = $sql->prepare("select * from `sis_item` where available=true ORDER BY id_item LIMIT ? ")) && true;
            $res &= $stmt->bind_param("i", $qty);
            $res &= $stmt->execute();
            $res &= $stmt->store_result();
            $res &= $stmt->bind_result($o->id_item, $o->name, $o->number, $o->description, $o->available, $o->id_category, $o->file_image, $o->cost);
            if (!$res) {
                return null;
            }
            $array = array();
            while ($resFetch = $stmt->fetch()) {
                if ($resFetch === false) {
                    return null;
                }
                //TODO FIND A BETTER WAY TO CLONE THIS!
                $clone = unserialize(serialize($o));
                $promo = promotion::findBestByItemId($sql, $clone->id_item);
                if ($promo) {
                    $clone->promotion = $promo;
                }
                array_push($array, $clone);
            }
            return $array;
        } catch (Exception $e) {
            //echo $e;
        } finally {
            $stmt->close();
        }
        return null;
    }

    /**
     * 
     * @param mysqli $sql
     * @param sis_item $o
     * @return int id generado 
     */
    public static function create($sql, $o) {
        if (!self::validate($o)) {
            return null;
        }
        try {
            $res = ($stmt = $sql->prepare("INSERT INTO `sis_item` (`name`, `number`, `description`, `available`, `id_category`, `file_image`, `cost`) VALUES (?, ?, ?, ?, ?, ?, ?)")) && true;
            $res &= $stmt->bind_param("sisiisd", $o->name, $o->number, $o->description, $o->available, $o->id_category, $o->file_image, $o->cost);
            $res &= $stmt->execute();
            if (!$res) {
                return null;
            }
            $id = $stmt->insert_id;
            return $id;
        } catch (Exception $e) {
            //echo $e;
        } finally {
            $stmt->close();
        }
        return null;
    }

    /**
     * @param mysqli $sql Conexion de base de datos
     * @param sis_item $o Objeto con datos del usuario
     * @return boolean
     */
    public static function update($sql, $o) {
        if (!self::validate($o) || !$o->id_item) {
            return false;
        }
        try {
            $res = ($stmt = $sql->prepare("UPDATE `sis_item` set `name`=?, `number`=?, `description`=?, `available`=?, `id_category`=?, `file_image`=?, `cost`=? where id_item=?")) && true;
            $res &= $stmt->bind_param("sisissdi", $o->name, $o->number, $o->description, $o->available, $o->id_category, $o->file_image, $o->cost, $o->id_item);
            $res &= $stmt->execute();
            return $res;
        } catch (Exception $e) {
            //echo $e;
        } finally {
            $stmt->close();
        }
        return false;
    }

}
