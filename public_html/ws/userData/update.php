<?php

/**
 * 
 */
require_once '../db/model/sis_user.php';
require_once '../db/model/sis_user_data.php';
require_once '../service.php';
header('Content-type: application/json');


$method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : null;

if ($method == "POST") {
    //Parse Input
    $inputJSON = file_get_contents('php://input');
    $input = json_decode($inputJSON, FALSE);

    if (!$input) {
        service::setResponseAndExit(HTTPCodes::Bad_Request, MsgStatus::ERROR, 'Parameter syntax error');
    }
    $o = sis_user_data::fromVar($input);

    if (!sis_user_data::validate($o)) {
        service::setResponseAndExit(HTTPCodes::Bad_Request, MsgStatus::ERROR, 'Required parameters not found or invalid');
    }

    $sql = db::getInstanceDefault();
    if (!$sql) {
        service::setResponseAndExit(HTTPCodes::Internal_Server_Error, MsgStatus::ERROR, 'No database');
    }
    $user = sis_user::findById($sql, $o->id_user_data);
    if (!$user || $user->id_user == null) {
        service::setResponseAndExit(HTTPCodes::Bad_Request, MsgStatus::ERROR, 'Referenced user do not exist');
    }
    $res = null;
    $old = sis_user_data::findById($sql, $o->id_user_data);
    if (!$old) {
        service::setResponseAndExit(HTTPCodes::Internal_Server_Error, MsgStatus::ERROR, 'No database');
    }
    if ($old->id_user_data == null) {
        $res = sis_user_data::create($sql, $o);
    } else {
        $res = sis_user_data::update($sql, $o);
        if ($res) {
            $res = $o->id_user_data;
        }
    }
    $sql->close();
    if ($res) {
        service::setResponse(HTTPCodes::OK, MsgStatus::OK, "User data updated", $res);
    } else {
        service::setResponse(HTTPCodes::Internal_Server_Error, MsgStatus::ERROR, "Server error at update");
    }
    return;
}
service::setResponse(HTTPCodes::Method_Not_Allowed, MsgStatus::ERROR, 'This method type is not allowed');
