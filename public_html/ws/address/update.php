<?php

/**
 * 
 */
require_once '../db/model/sis_user_data.php';
require_once '../db/model/address.php';
require_once '../service.php';
header('Content-type: application/json');


$method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : null;

if ($method == "POST") {
    //Parse Input
    $inputJSON = file_get_contents('php://input');
    $input = json_decode($inputJSON, FALSE);

    if (!$input) {
        service::setResponseAndExit(HTTPCodes::Bad_Request, MsgStatus::ERROR, 'Input syntax error');
    }
    $o = address::fromVar($input);

    if (!address::validate($o) || !$o->id_user) {
        service::setResponseAndExit(HTTPCodes::Bad_Request, MsgStatus::ERROR, 'Required parameters not found or invalid');
    }

    $sql = db::getInstanceDefault();
    if (!$sql) {
        service::setResponseAndExit(HTTPCodes::Internal_Server_Error, MsgStatus::ERROR, 'No database');
    }
    $user = sis_user_data::findById($sql, $o->id_user);
    if (!$user || $user->id_user_data == null) {
        service::setResponseAndExit(HTTPCodes::Bad_Request, MsgStatus::ERROR, 'Personal information must be filled first');
    }
    if ($o->id_address == null) {
        $res = address::create($sql, $o);
    } else {
        $res = address::update($sql, $o);
        if ($res) {
            $res = $o->id_address;
        }
    }
    $sql->close();
    if ($res) {
        service::setResponse(HTTPCodes::Created, MsgStatus::OK, 'Adress updated', $res);
    } else {
        service::setResponse(HTTPCodes::Internal_Server_Error, MsgStatus::ERROR, 'Update failed');
    }
    return;
}
service::setResponse(HTTPCodes::Method_Not_Allowed, MsgStatus::ERROR, 'This method type is not allowed');
