<?php

/**
 * 
 */
require_once '../db/model/sis_item.php';
require_once '../service.php';
header('Content-type: application/json');


$method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : null;
if ($method == "POST") {
    //Parse Input
    $inputJSON = null;
    $fileData = null;

    if (isset($_POST["model"])) {
        $inputJSON = $_POST["model"];
    }
    if (isset($_POST["fileData"])) {
        $fileData = $_POST["fileData"];
    }
    $updateFile = true;
    if (!isset($_FILES['file'])) {
        $updateFile = false;
    }

    if ($updateFile && $_FILES['file']['error'] != 0) {
        service::setResponseAndExit(HTTPCodes::Bad_Request, MsgStatus::ERROR, 'Image file upload error');
    }
    if ($updateFile && !is_uploaded_file($_FILES['file']['tmp_name'])) {
        service::setResponseAndExit(HTTPCodes::Bad_Request, MsgStatus::ERROR, 'Dangerous file uploaded');
    }

    $input = json_decode($inputJSON, FALSE);

    if (!$input) {
        service::setResponseAndExit(HTTPCodes::Bad_Request, MsgStatus::ERROR, 'Parameter syntax error');
    }
    $o = sis_item::fromVar($input);

    if (!sis_item::validate($o) || !$o->id_item) {
        service::setResponseAndExit(HTTPCodes::Bad_Request, MsgStatus::ERROR, 'Required parameters not found or invalid');
    }
    $sql = db::getInstanceDefault();
    if (!$sql) {
        service::setResponseAndExit(HTTPCodes::Internal_Server_Error, MsgStatus::ERROR, 'No database');
    }

    $res = sis_item::update($sql, $o);

    $sql->close();
    if ($res) {
        if ($updateFile) {
            //Move file
            $info = pathinfo($_FILES['file']['name']);
            $newname = $o->id_item . "." . $o->file_image;
            $target = '../../items/' . $newname;
            //TODO solo reemplazara si el archivo tiene la misma extension, sino el archivo anterior quedara residual hay q arreglarlo
            if (file_exists($target)) {
                unlink($target); //remove the file
            }
            move_uploaded_file($_FILES['file']['tmp_name'], $target);
        }
        service::setResponse(HTTPCodes::OK, MsgStatus::OK, "Item updated", $res);
    } else {
        service::setResponse(HTTPCodes::Internal_Server_Error, MsgStatus::ERROR, "Server error at update");
    }
    return;
}
service::setResponse(HTTPCodes::Method_Not_Allowed, MsgStatus::ERROR, 'This method type is not allowed');
