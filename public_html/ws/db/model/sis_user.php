<?php

require_once dirname(__DIR__) . '../db.php';

/**
 * Description of sis_user
 *
 * @author HadesDX
 */
class sis_user {

    public $id_user;
    public $name;
    public $pass;

    /**
     * Construye el objeto sis_category desde un objecto de json_decode
     * @param object $v
     * @return \sis_user
     */
    public static function fromVar($v) {
        if (!$v) {
            return null;
        }
        $o = new sis_user();
        if (isset($v->id_user)) {
            $o->id_user = $v->id_user;
        }
        if (isset($v->name)) {
            $o->name = $v->name;
        }
        if (isset($v->pass)) {
            $o->pass = $v->pass;
        }
        return $o;
    }

    /**
     * 
      @param mysqli $sql Conexion de base de datos
     * @param int $id
     * @return boolean
     */
    public static function dropById($sql, $id) {
        if (!$sql || !$id) {
            return false;
        }
        try {
            $res = ($s = $sql->prepare("delete from `sis_user` where id_user=?")) && true;
            $res &= $s->bind_param("i", $id);
            $res &= $s->execute();
            return $res;
        } catch (Exception $e) {
            //echo $e;
        } finally {
            $s->close();
        }
        return false;
    }

    /**
     * @param mysqli $sql Conexion de base de datos
     * @param int $id
     * @return \sis_user
     */
    public static function findById($sql, $id) {
        if (!$sql || !$id) {
            return null;
        }
        try {
            $u = new sis_user();
            $res = ($stmt = $sql->prepare("select * from `sis_user` where id_user=?")) && true;
            $res &= $stmt->bind_param("i", $id);
            $res &= $stmt->execute();
            $res &= $stmt->bind_result($u->id_user, $u->name, $u->pass);
            $res &= $stmt->store_result();
            $resFetch = $stmt->fetch();
            if (!$res || $resFetch === false) {
                return null;
            }
            return $u;
        } catch (Exception $e) {
            //echo $e;
        } finally {
            $stmt->close();
        }
        return null;
    }

    /**
     * @param mysqli $sql Conexion de base de datos
     * @param sis_user $user Objeto con datos del usuario
     * @return int ID usuario creado
     */
    public static function create($sql, $user) {
        if (!$user || !$user->name || !$user->pass) {
            return null;
        }
        try {
            $res = ($stmt = $sql->prepare("INSERT INTO `sis_user`(`name`,`pass`)VALUES(?,SHA2(?,256))")) && true;
            $res &= $stmt->bind_param("ss", $user->name, $user->pass);
            $res &= $stmt->execute();
            if (!$res) {
                return null;
            }
            $id = $stmt->insert_id;
            return $id;
        } catch (Exception $e) {
            //echo $e;
        } finally {
            $stmt->close();
        }
        return null;
    }

    /**
     * @param mysqli $sql Conexion de base de datos
     * @param sis_user $user Objeto con datos del usuario
     * @return boolean Or null 
     */
    public static function update($sql, $user) {
        if (!$user || $user->id_user == null || $user->pass == null) {
            return null;
        }
        try {
            $res = ($stmt = $sql->prepare("UPDATE `sis_user` set `pass`=SHA2(?,256) where id_user=?")) && true;
            $res &= $stmt->bind_param("si", $user->pass, $user->id_user);
            $res &= $stmt->execute();
            if (!$res) {
                return null;
            }
            return $res;
        } catch (Exception $e) {
            //echo $e;
        } finally {
            $stmt->close();
        }
        return false;
    }

    /**
     * 
     * @param mysqli $sql Conexion de base de datos
     * @param sis_user $user Objeto con datos del usuario
     * @return int ID del usuario o null si no hay un usuario coincidente
     */
    public static function login($sql, $user) {
        if (!$user || !$user->name || !$user->pass) {
            return null;
        }
        try {
            $res = ($stmt = $sql->prepare("select id_user from `sis_user` where name=? and pass=SHA2(?,256)")) && true;
            $res &= $stmt->bind_param("ss", $user->name, $user->pass);
            $res &= $stmt->execute();
            $res &= $stmt->bind_result($id);
            $res &= $stmt->fetch();
            if (!$res) {
                return null;
            }
            return $id;
        } catch (Exception $e) {
            //echo $e;
        } finally {
            $stmt->close();
        }
        return null;
    }

}
