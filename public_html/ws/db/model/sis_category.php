<?php

require_once dirname(__DIR__) . '../db.php';

/**
 * Description of sis_category
 *
 * @author HadesDX
 */
class sis_category {

    //PRIMARY KEY
    public $id_category;
    public $name;
    public $description;
    public $subcategoria;

    public function __clone() {
        
    }

    /**
     * Construye el objeto sis_category desde un objecto de json_decode
     * @param object $v
     * @return \sis_category
     */
    public static function fromVar($v) {
        if (!$v) {
            return null;
        }
        $o = new sis_category();
        if (isset($v->id_category)) {
            $o->id_category = $v->id_category;
        }
        if (isset($v->name)) {
            $o->name = $v->name;
        }
        if (isset($v->description)) {
            $o->description = $v->description;
        }
        if (isset($v->subcategoria)) {
            $o->subcategoria = $v->subcategoria;
        }
        return $o;
    }

    /**
     * Valida la estructura para ser usada en la base de datos.
     * No valida llaves.
     * 
     * @param sis_category $o Instancia a validar
     * @return boolean true on valid
     */
    public static function validate($o = null) {
        if (!$o || $o->name == null) {
            return false;
        }
        return true;
    }

    /**
     * 
     * @param mysqli $sql Conexion de base de datos
     * @param int $id id
     * @return boolean
     */
    public static function dropById($sql = null, $id = null) {
        if (!$sql || !$id) {
            return false;
        }
        try {
            $res = ($stmt = $sql->prepare("delete from `sis_category` where id_category=?")) && true;
            $res &= $stmt->bind_param("i", $id);
            $res &= $stmt->execute();
            if (!$res) {
                return false;
            }
            return true;
        } catch (Exception $e) {
            //echo $e;
        } finally {
            $stmt->close();
        }
        return false;
    }

    /**
     * 
     * @param mysqli $sql Conexion de base de datos
     * @param int $id id
     * @return sis_category
     */
    public static function findById($sql, $id) {
        if (!$sql || !$id) {
            return null;
        }
        try {
            $o = new sis_category();
            $res = ($stmt = $sql->prepare("select * from `sis_category` where id_category=?")) && true;
            $res &= $stmt->bind_param("i", $id);
            $res &= $stmt->execute();
            $res &= $stmt->bind_result($o->id_category, $o->name, $o->description, $o->subcategoria);
            $res &= $stmt->store_result();
            $resFetch = $stmt->fetch();
            if (!$res || $resFetch === false) {
                return null;
            }
            return $o;
        } catch (Exception $e) {
            //echo $e;
        } finally {
            $stmt->close();
        }
        return null;
    }

    /**
     * 
     * @param mysqli $sql Conexion de base de datos
     * @return array of sis_category
     */
    public static function findAll($sql) {
        if (!$sql) {
            return null;
        }
        try {
            $o = new sis_category();
            $res = ($stmt = $sql->prepare("SELECT * FROM `sis_category` order by `name`,`subcategoria`")) && true;
            $res &= $stmt->execute();
            $res &= $stmt->bind_result($o->id_category, $o->name, $o->description, $o->subcategoria);
            if (!$res) {
                return null;
            }
            $array = array();
            while ($resFetch = $stmt->fetch()) {
                //TODO FIND A BETTER WAY TO CLONE THIS!
                $clone = unserialize(serialize($o));
                array_push($array, $clone);
                if ($resFetch === false) {
                    return null;
                }
            }
            return $array;
        } catch (Exception $e) {
            //echo $e;
        } finally {
            $stmt->close();
        }
        return null;
    }

    /**
     * @param mysqli $sql
     * @param sis_category $o
     * @return int id generado 
     */
    public static function create($sql, $o) {
        if (!self::validate($o)) {
            return null;
        }
        try {
            $res = ($stmt = $sql->prepare("INSERT INTO `sis_category` (`name`, `description`, `subcategoria`) VALUES (?, ?, ?)")) && true;
            $res &= $stmt->bind_param("sss", $o->name, $o->description, $o->subcategoria);
            $res &= $stmt->execute();
            if (!$res) {
                return null;
            }
            $id = $stmt->insert_id;
            return $id;
        } catch (Exception $e) {
            //echo $e;
        } finally {
            $stmt->close();
        }
        return null;
    }

    /**
     * @param mysqli $sql Conexion de base de datos
     * @param sis_category $o Objeto con datos del usuario
     * @return boolean
     */
    public static function update($sql, $o) {
        if (!self::validate($o) || !$o->id_category) {
            return false;
        }
        try {
            $res = ($stmt = $sql->prepare("UPDATE `sis_category` set `name`=?, `description`=?, `subcategoria`=? where id_category=?")) && true;
            $res &= $stmt->bind_param("sssi", $o->name, $o->description, $o->subcategoria, $o->id_category);
            $res &= $stmt->execute();
            return $res;
        } catch (Exception $e) {
            //echo $e;
        } finally {
            if ($stmt) {
                $stmt->close();
            }
        }
        return false;
    }

}
