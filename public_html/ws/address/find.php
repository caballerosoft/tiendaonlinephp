<?php

/**
 * 
 */
require_once '../db/model/address.php';
require_once '../service.php';
header('Content-type: application/json');


$method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : null;

if ($method == "POST") {
    //Parse Input
    $inputJSON = file_get_contents('php://input');
    $input = json_decode($inputJSON, FALSE);

    if (!$input) {
        service::setResponseAndExit(HTTPCodes::Bad_Request, MsgStatus::ERROR, 'Input syntax error');
    }
    $o = address::fromVar($input);
    if (!$o || $o->id_address == null) {
        service::setResponseAndExit(HTTPCodes::Bad_Request, MsgStatus::ERROR, 'Incomplite input');
    }

    $sql = db::getInstanceDefault();
    if (!$sql) {
        service::setResponseAndExit(HTTPCodes::Internal_Server_Error, MsgStatus::ERROR, 'No database');
    }
    $res = address::findById($sql, $o->id_address);
    $sql->close();
    if ($res) {
        if ($res->id_address) {
            service::setResponse(HTTPCodes::OK, MsgStatus::OK, "userAddress", $res);
        } else {
            service::setResponse(HTTPCodes::OK, MsgStatus::OK, "userAddress", null);
        }
    } else {
        service::setResponse(HTTPCodes::Internal_Server_Error, MsgStatus::ERROR, 'Search failed');
    }
    return;
}
service::setResponse(HTTPCodes::Method_Not_Allowed, MsgStatus::ERROR, 'This method type is not allowed');
