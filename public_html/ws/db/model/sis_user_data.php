<?php

require_once dirname(__DIR__) . '../db.php';

/**
 * Description of sis_user
 *
 * @author HadesDX
 */
class sis_user_data {

    public $name;
    public $middle_name;
    public $last_name;
    public $email;
    public $comment;
    public $phone;
    public $status;
    public $id_user_data;

    /**
     * Construye el objeto sis_user_data desde un objecto de json_decode
     * @param object $v
     * @return \sis_user_data
     */
    public static function fromVar($v) {
        if (!$v) {
            return null;
        }
        $o = new sis_user_data();
        if (isset($v->name)) {
            $o->name = $v->name;
        }
        if (isset($v->middle_name)) {
            $o->middle_name = $v->middle_name;
        }
        if (isset($v->last_name)) {
            $o->last_name = $v->last_name;
        }
        if (isset($v->email)) {
            $o->email = $v->email;
        }
        if (isset($v->comment)) {
            $o->comment = $v->comment;
        }
        if (isset($v->phone)) {
            $o->phone = $v->phone;
        }
        if (isset($v->status)) {
            $o->status = $v->status;
        }
        if (isset($v->id_user_data)) {
            $o->id_user_data = $v->id_user_data;
        }
        return $o;
    }

    /**
     * Valida la estructura para ser usada en la base de datos.
     * No valida llaves.
     * 
     * @param sis_user_data $o Instancia a validar
     * @return boolean True on valid
     */
    public static function validate($o = null) {
        if (!$o || $o->name == null || $o->last_name == null || $o->email == null || $o->status == null) {
            return false;
        }
        return true;
    }

    /**
     * 
     * @param mysqli $sql Conexion de base de datos
     * @param int $id id de usuario
     * @return boolean
     */
    public static function dropById($sql = null, $id = null) {
        if (!$sql || !$id) {
            return false;
        }
        try {
            $res = ($s = $sql->prepare("delete from `sis_user_data` where id_user_data=?")) && true;
            $res &= $s->bind_param("i", $id);
            $res &= $s->execute();
            if (!$res) {
                return false;
            }
            return true;
        } catch (Exception $e) {
            //echo $e;
        } finally {
            $s->close();
        }
        return false;
    }

    /**
     * 
     * @param mysqli $sql Conexion de base de datos
     * @param int $id
     * @return sis_user_data 
     */
    public static function findById($sql, $id) {
        if (!$sql || !$id) {
            return null;
        }
        try {
            $o = new sis_user_data();
            $res = ($s = $sql->prepare("select * from `sis_user_data` where id_user_data=?")) && true;
            $res &= $s->bind_param("i", $id);
            $res &= $s->execute();
            $res &= $s->bind_result($o->name, $o->middle_name, $o->last_name, $o->email, $o->comment, $o->phone, $o->status, $o->id_user_data);
            $resFetch = $s->fetch();
            if (!$res || $resFetch === false) {
                return null;
            }
            return $o;
        } catch (Exception $e) {
            //echo $e;
        } finally {
            $s->close();
        }
        return null;
    }

    /**
     * @param mysqli $sql
     * @param sis_user_data $o
     * @return int id generado 
     */
    public static function create($sql, $o) {
        if (!self::validate($o)) {
            return null;
        }
        try {
            $res = ($s = $sql->prepare("INSERT INTO `sis_user_data` (`name`, `middle_name`, `last_name`, `email`, `comment`, `phone`, `status`,`id_user_data`) VALUES (?, ?, ?, ?, ?, ?, ?, ?)")) && true;
            $res &= $s->bind_param("ssssssii", $o->name, $o->middle_name, $o->last_name, $o->email, $o->comment, $o->phone, $o->status, $o->id_user_data);
            $res &= $s->execute();
            if (!$res) {
                return null;
            }
            return $o->id_user_data;
        } catch (Exception $e) {
            //echo $e;
        } finally {
            $s->close();
        }
        return null;
    }

    /**
     * @param mysqli $sql Conexion de base de datos
     * @param sis_user_data $o Objeto con datos del usuario
     * @return boolean
     */
    public static function update($sql, $o) {
        if (!self::validate($o) || !$o->id_user_data) {
            return false;
        }
        try {
            $res = ($s = $sql->prepare("UPDATE `sis_user_data` set `name`=?, `middle_name`=?, `last_name`=?, `email`=?, `comment`=?, `phone`=?, `status`=? where id_user_data=?")) && true;
            $res &= $s->bind_param("ssssssii", $o->name, $o->middle_name, $o->last_name, $o->email, $o->comment, $o->phone, $o->status, $o->id_user_data);
            $res &= $s->execute();
            return $res;
        } catch (Exception $e) {
            //echo $e;
        } finally {
            $s->close();
        }
        return false;
    }

}
