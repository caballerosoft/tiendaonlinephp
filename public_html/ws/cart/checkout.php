<?php

/**
 * 
 */
require_once '../db/model/register_sale.php';
require_once '../db/model/sale.php';
require_once '../db/model/sis_item.php';
require_once '../service.php';
header('Content-type: application/json');


$method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : null;

if ($method == "POST") {
    //Parse Input
    $inputJSON = file_get_contents('php://input');
    $input = json_decode($inputJSON, FALSE);
    if (!$input) {
        service::setResponseAndExit(HTTPCodes::Bad_Request, MsgStatus::ERROR, 'Parameter syntax error');
    }

    $sql = db::getInstanceDefault();
    if (!$sql) {
        service::setResponseAndExit(HTTPCodes::Internal_Server_Error, MsgStatus::ERROR, 'No database');
    }

    //Revisar que las cantidades solicitadas sean validas
    $continue = true;
    foreach ($input->cart as &$v) {
        $id = $v->item->id_item;
        $obj = sis_item::findById($sql, $id);
        $v->item->number = $obj->number;
        if ($v->qty > $obj->number) {
            $v->qty = $obj->number;
            $continue = false;
        }
    }
    unset($v);
    if (!$continue) {
        service::setResponseAndExit(HTTPCodes::Bad_Request, MsgStatus::ERROR, 'Not enought items to fill order', $input);
    }
    unset($v);
    $sale = new sale();
    $sale->id_user = $input->user->id_user;
    $sale->status = status::PROCESO;
    $sale->num_bill = md5(mt_rand());

    $res = sale::create($sql, $sale);
    if (!$res) {
        service::setResponseAndExit(HTTPCodes::Internal_Server_Error, MsgStatus::ERROR, 'Sale create error');
    }
    $sale->id_sale = $res;
    $res = sale::findById($sql, $sale->id_sale);
    if (!$res) {
        service::setResponseAndExit(HTTPCodes::Internal_Server_Error, MsgStatus::ERROR, 'Sale search error');
    }
    //Se creo una entrada de venta
    $sale = $res;


    //si son validas descontar
    foreach ($input->cart as &$v) {
        $id = $v->item->id_item;
        $qty = $v->qty;
        $rs = new register_sale();
        $rs->id_item = $id;
        $rs->id_sale = $sale->id_sale;
        $rs->number = $qty;
        if (isset($v->item->promotion)) {
            $rs->id_promotion = $v->item->promotion->id_promotion;
        }
        $res = register_sale::create($sql, $rs);
        if (!$res) {
            service::setResponseAndExit(HTTPCodes::Internal_Server_Error, MsgStatus::ERROR, 'Sale register error', $v);
        }
        $obj = sis_item::findById($sql, $id);
        $obj->number = $obj->number - $qty;
        sis_item::update($sql, $obj);
    }
    $sql->close();
    service::setResponse(HTTPCodes::OK, MsgStatus::OK, "Sale recorded", $sale->id_sale);
    return;
}
service::setResponse(HTTPCodes::Method_Not_Allowed, MsgStatus::ERROR, 'This method type is not allowed');
