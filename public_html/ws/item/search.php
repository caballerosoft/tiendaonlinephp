<?php

/**
 * 
 */
require_once '../db/model/sis_item.php';
require_once '../db/model/promotion.php';
require_once '../db/model/sis_category.php';
require_once '../service.php';
header('Content-type: application/json');


$method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : null;

if ($method == "POST") {
    //Parse Input
    $inputJSON = file_get_contents('php://input');
    $input = json_decode($inputJSON, FALSE);

    if (!$input) {
        service::setResponseAndExit(HTTPCodes::Bad_Request, MsgStatus::ERROR, 'Parameter syntax error');
    }


    if (!isset($input->q) || !$input->q) {
        service::setResponseAndExit(HTTPCodes::Bad_Request, MsgStatus::ERROR, 'Required parameters not found or invalid');
    }
    $sql = db::getInstanceDefault();
    if (!$sql) {
        service::setResponseAndExit(HTTPCodes::Internal_Server_Error, MsgStatus::ERROR, 'No database');
    }
    $res = sis_item::search($sql, $input->q);

    if ($res !== null && $res !== false) {
        service::setResponse(HTTPCodes::OK, MsgStatus::OK, 'Items', $res);
    } else {
        service::setResponse(HTTPCodes::Internal_Server_Error, MsgStatus::ERROR, 'Items error at search');
    }
    $sql->close();
    return;
}
service::setResponse(HTTPCodes::Method_Not_Allowed, MsgStatus::ERROR, 'This method type is not allowed');
