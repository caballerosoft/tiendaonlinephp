<?php

require_once './db/config.php';
require_once './db/model/sis_user.php';

echo get_include_path();
echo "<br>";
$sql = new mysqli($dbConfig->host, $dbConfig->user, $dbConfig->passwd, $dbConfig->database);
if ($sql->connect_errno) {
    echo $sql->connect_error;
    return;
}

echo "Bases de datos <br>";
$res = $sql->query("show databases");
if ($res) {
    /* Obtener la información del campo para todas las columnas */
    while ($info_campo = $res->fetch_assoc()) {

        foreach ($info_campo as $valor) {
            echo var_dump($valor);
        }
    }
}
$res->free();
echo "Tablas <br>";
$res = $sql->query("show tables");
if ($res) {
    /* Obtener la información del campo para todas las columnas */
    while ($info_campo = $res->fetch_assoc()) {

        foreach ($info_campo as $valor) {
            echo var_dump($valor);
        }
    }
}
$res->free();


$sql->close();

echo sis_user::create(null);
