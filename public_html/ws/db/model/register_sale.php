<?php

require_once dirname(__DIR__) . '/model/promotion.php';
require_once dirname(__DIR__) . '/model/sis_item.php';
require_once dirname(__DIR__) . '../db.php';

/**
 * Description of register_sale
 *
 * @author HadesDX
 */
class register_sale {

    public $id_register_sale;
    public $id_sale;
    public $id_item;
    public $id_promotion;
    public $number;

    /**
     * Valida la estructura para ser usada en la base de datos.
     * No valida llaves.
     * 
     * @param register_sale $o Instancia a validar
     * @return boolean true on valid
     */
    public static function validate($o = null) {
        if (!$o || $o->number == null) {
            return false;
        }
        return true;
    }

    /**
     * 
     * @param mysqli $sql Conexion de base de datos
     * @param int $id id sale
     * @return array of register_sale
     */
    public static function findBySaleID($sql, $id) {
        if (!$sql || !$id) {
            return null;
        }
        try {
            $o = new register_sale();
            $res = ($stmt = $sql->prepare("select * from `register_sale` where id_sale=?")) && true;
            $res &= $stmt->bind_param("i", $id);
            $res &= $stmt->execute();
            $res &= $stmt->store_result();
            $res &= $stmt->bind_result($o->id_register_sale, $o->id_sale, $o->id_item, $o->id_promotion, $o->number);
            if (!$res) {
                return null;
            }
            $array = array();
            while ($resFetch = $stmt->fetch()) {
                if ($resFetch === false) {
                    return null;
                }
                //TODO FIND A BETTER WAY TO CLONE THIS!
                $clone = unserialize(serialize($o));
                $item = sis_item::findById($sql, $clone->id_item);
                if ($item) {
                    $clone->item = $item;
                }
                $promo = promotion::findById($sql, $clone->id_promotion);
                if ($promo) {
                    $clone->promotion = $promo;
                }
                array_push($array, $clone);
            }
            return $array;
        } catch (Exception $e) {
            //echo $e;
        } finally {
            $stmt->close();
        }
        return null;
    }

    /**
     * 
     * @param mysqli $sql
     * @param register_sale $o
     * @return int id generado 
     */
    public static function create($sql, $o) {
        if (!self::validate($o)) {
            return null;
        }
        try {
            $res = ($stmt = $sql->prepare("INSERT INTO `register_sale` (`id_sale`, `id_item`, `id_promotion`, `number`) VALUES ( ?, ?, ?, ?)")) && true;
            $res &= $stmt->bind_param("iiii", $o->id_sale, $o->id_item, $o->id_promotion, $o->number);
            $res &= $stmt->execute();
            if (!$res) {
                return null;
            }
            $id = $stmt->insert_id;
            return $id;
        } catch (Exception $e) {
            //echo $e;
        } finally {
            $stmt->close();
        }
        return null;
    }

}
