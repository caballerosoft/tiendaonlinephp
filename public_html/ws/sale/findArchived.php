<?php

/**
 * 
 */
require_once '../db/model/sale.php';
require_once '../db/model/sis_user.php';
require_once '../service.php';
header('Content-type: application/json');


$method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : null;

if ($method == "POST") {
    $inputJSON = file_get_contents('php://input');
    $input = json_decode($inputJSON, FALSE);
    if (!$input) {
        service::setResponseAndExit(HTTPCodes::Bad_Request, MsgStatus::ERROR, 'Parameter syntax error');
    }
    $user = sis_user::fromVar($input);
    if (!$user->id_user) {
        service::setResponseAndExit(HTTPCodes::Bad_Request, MsgStatus::ERROR, 'Required parameters not found or invalid');
    }

    $sql = db::getInstanceDefault();
    if (!$sql) {
        service::setResponseAndExit(HTTPCodes::Internal_Server_Error, MsgStatus::ERROR, 'No database');
    }
    $res = sale::findArchived($sql, $user->id_user);
    $sql->close();
    if ($res !== null) {
        service::setResponse(HTTPCodes::OK, MsgStatus::OK, "Open orders", $res);
    } else {
        service::setResponse(HTTPCodes::Internal_Server_Error, MsgStatus::ERROR, 'Search failed');
    }
    return;
}
service::setResponse(HTTPCodes::Method_Not_Allowed, MsgStatus::ERROR, 'This method type is not allowed');
