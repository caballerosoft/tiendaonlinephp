<?php

/**
 * Borra un usuario
 */
require_once '../db/model/sis_user.php';
require_once '../service.php';
header('Content-type: application/json');


$method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : null;

if ($method == "POST") {
    $inputJSON = file_get_contents('php://input');
    $input = json_decode($inputJSON, FALSE);
    if (!isset($input->id_user)) {
        service::setResponseAndExit(HTTPCodes::Bad_Request, MsgStatus::ERROR, "Input syntax error or invalid input");
    }

    $sql = db::getInstanceDefault();
    if (!$sql) {
        service::setResponseAndExit(HTTPCodes::Internal_Server_Error, MsgStatus::ERROR, 'No database');
    }
    $res = sis_user::dropById($sql, $input->id_user);
    $sql->close();
    if ($res) {
        service::setResponse(HTTPCodes::OK, MsgStatus::OK, 'User deleted');
    } else {
        service::setResponse(HTTPCodes::Internal_Server_Error, MsgStatus::ERROR, 'Error at delete');
    }
    return;
}
service::setResponse(HTTPCodes::Method_Not_Allowed, MsgStatus::ERROR, 'This method type is not allowed');
