<?php

/**
 * Registra un nuevo usuario
 */
require_once '../db/model/sis_user.php';
require_once '../service.php';
header('Content-type: application/json');


$method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : null;

if ($method == "POST") {
    $inputJSON = file_get_contents('php://input');
    $input = json_decode($inputJSON, FALSE);
    if (!isset($input->name) || !isset($input->pass)) {
        service::setResponseAndExit(HTTPCodes::Bad_Request, MsgStatus::ERROR, 'Input syntax error or invalid input');
    }
    $u = new sis_user();
    $u->name = $input->name;
    $u->pass = $input->pass;
    if ($u->name === 'admin') {
        service::setResponseAndExit(HTTPCodes::Bad_Request, MsgStatus::ERROR, 'admin is a reserved user name');
    }
    $sql = db::getInstanceDefault();
    if (!$sql) {
        service::setResponseAndExit(HTTPCodes::Internal_Server_Error, MsgStatus::ERROR, 'No database');
    }
    //TOTO revisar q no aya un usuario con ese nombre
    //$exist= sis_user::
    $res = sis_user::create($sql, $u);
    $sql->close();
    if ($res) {
        service::setResponse(HTTPCodes::Created, MsgStatus::OK, 'User registered', $res);
    } else {
        service::setResponse(HTTPCodes::Internal_Server_Error, MsgStatus::ERROR, 'Could not register user');
    }
    return;
}
service::setResponse(HTTPCodes::Method_Not_Allowed, MsgStatus::ERROR, 'This method type is not allowed');
