<?php

/**
 * 
 */
require_once '../db/model/register_sale.php';
require_once '../db/model/sale.php';
require_once '../db/model/sis_item.php';
require_once '../db/model/promotion.php';
require_once '../service.php';
header('Content-type: application/json');


$method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : null;

if ($method == "POST") {
    //Parse Input
    $inputJSON = file_get_contents('php://input');
    $input = json_decode($inputJSON, FALSE);

    if (!$input) {
        service::setResponseAndExit(HTTPCodes::Bad_Request, MsgStatus::ERROR, 'Parameter syntax error');
    }
    $o = sale::fromVar($input);
    if (!$o || $o->id_sale == null) {
        service::setResponseAndExit(HTTPCodes::Bad_Request, MsgStatus::ERROR, 'Required parameters not found or invalid');
    }

    $sql = db::getInstanceDefault();
    if (!$sql) {
        service::setResponseAndExit(HTTPCodes::Bad_Request, MsgStatus::ERROR, 'No database');
    }
    $res = sale::findById($sql, $o->id_sale);

    if ($res) {
        if (!$res->id_sale) {
            //La venta no existe
            $res = null;
        } else {
            //La venta existe, buscar registros de venta
            $res->register_sale = register_sale::findBySaleID($sql, $res->id_sale);
        }
        service::setResponse(HTTPCodes::OK, MsgStatus::OK, 'Sale', $res);
    } else {
        service::setResponse(HTTPCodes::Internal_Server_Error, MsgStatus::ERROR, 'Sale error at search');
    }
    $sql->close();
    return;
}
service::setResponse(HTTPCodes::Method_Not_Allowed, MsgStatus::ERROR, 'This method type is not allowed');
