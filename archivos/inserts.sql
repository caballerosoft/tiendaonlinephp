####### USER ADMIN
INSERT INTO `mydb`.`sis_user` (`name`, `pass`) VALUES ('admin', SHA2('admin',256));
####### CATEGORIAS
INSERT INTO `mydb`.`sis_category` (`name`, `description`, `subcategoria`) VALUES 
( 'Sports', 'Exercise and other things', 'Fitness'),
( 'Sports', 'Soccer shoes, clothes and more', 'Soccer'),
( 'Sports', 'Everything for running', 'Running'),
( 'Beauty', 'The best brands, for face and body', 'Cremes'),
( 'Beauty', 'Most popular perfume & cologne brands', 'Perfumes'),
( 'Beauty', 'Everything you need', 'Tools'),
( 'Technology', 'Cables, adapters and devices', 'Audio'),
( 'Technology', 'Desktops, laptops and components', 'Computers'),
( 'Technology', 'From every day use, to hight end devices',  'Smartphones'),
( 'Technology', 'Console retail games', 'Games'),
( 'Technology', 'From small to big, plane and curve, even 3D!', 'TV');
####### Item
INSERT INTO `mydb`.`sis_item` (`name`, `number`, `description`, `available`, `id_category`, `file_image`, `cost`) VALUES 
('Curl Bar', 3, 'Perfect addition to any home gym and easy to store away after a good, hard work out.', true, 1, 'jpg', 130.45),
('Mercurial Victory VI', 0, 'Nike Men\'s Mercurial Victory VI Dynamic Fit FG Soccer Shoe Cleat', false, 2, 'jpg', 140.99),
('Revolution 3', 0, 'NIKE Men\'s Revolution 3 Running Shoe', true, 3, 'jpg', 89.0),
('Clarant B3', 7, '7oz jar of Pond\'s Clarant B3 Normal to Dry Dark Spot Correcting Cream', true, 4, 'jpg', 28.20),
('ASUS ROG G752VS-XB78K', 3, 'OC Edition 17.3-Inch Gaming Laptop (i7-6820HK, 64GB RAM w/512 GB SSD + 1TB, Windows 10), Copper Titanium', true, 8, 'jpg', 699.99);
###### Promotion
INSERT INTO `mydb`.`promotion` (`discount`, `begin`, `end`, `status`, `id_item`) VALUES
 (90, NOW(), NOW() + INTERVAL 3 DAY, true, 5),
 (80, NOW(), NOW() + INTERVAL 3 DAY, false, 5),
 (75, NOW(), NOW() + INTERVAL 3 DAY, true, 5),
 (85, NOW() - INTERVAL 3 DAY, NOW(), true, 3),
 (80, NOW(), NOW() + INTERVAL 3 DAY, true, 3);